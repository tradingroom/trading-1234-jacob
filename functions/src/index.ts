import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin'
admin.initializeApp(functions.config().firebase);

export const onPaymentComplete = functions.database.ref('/payments/{userId}/{paymentId}/Status')
    .onUpdate(async (change, context) => {
        const userId = context.params.userId;
        const paymentId = context.params.paymentId;
        const before = change.before.val();
        const after = change.after.val();

        // console.log(`userId: ${userId}`);
        // console.log(`paymentId: ${paymentId}`);

        if (before === 0 && after === 1) {
            // console.log('here saving user data');
            const updates: { [id: string]: any; } = {};

            const daysTemp = await admin.database().ref(`/payments/${userId}/${paymentId}/duration`).once('value');
            const days = daysTemp.val();

            const paidPeriod = new Date();
            paidPeriod.setDate(paidPeriod.getDate() + days);

            const paidPeriodMs = paidPeriod.getTime();

            const snap = await admin.database().ref(`/users/${userId}/expiration`).once('value');
            if (snap.exists()) {
                // console.log('expiration already exists');

                const expiration = snap.val();
                if (expiration >= new Date().getTime()) {
                    // console.log('expiration is ', expiration);

                    const currentDate = new Date(expiration);
                    currentDate.setDate(currentDate.getDate() + days);

                    // console.log('currentDate', currentDate.getTime());
                    updates[`/users/${userId}/expiration`] = currentDate.getTime();
                } else {
                    // console.log('else');
                    // console.log(paidPeriodMs);
                    updates[`/users/${userId}/expiration`] = paidPeriodMs;
                }
            } else {
                // console.log('expiration DOES NOT exist');
                // console.log(paidPeriodMs);
                updates[`/users/${userId}/expiration`] = paidPeriodMs;
            }

            updates[`/users/${userId}/roles/subscriber`] = true;
            updates[`/payments/${userId}/${paymentId}/StartDate`] = Date.now();
            updates[`/payments/${userId}/${paymentId}/EndDate`] = new Date(Date.now() + days * 24 * 60 * 60 * 1000).getTime()

            return admin.database().ref().update(updates);
        } else {
            return null;
        }
    });
