import { Component, OnInit, OnDestroy, Input, ComponentRef, Output, EventEmitter} from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from '../core/api.service';
import { CommonService } from '../core/common.service';
import { Globals } from '../core/globals';
import { Router } from '@angular/router';
import { UserService, UserPreferenceType } from '../core/user.service';
import { AuthService } from '../core/auth.service';
import { MovingAverage } from '../models/movingAverage';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
@Component({
  selector: 'app-malevels',
  templateUrl: './malevels.component.html',
  styleUrls: ['./malevels.component.scss']
})
export class MalevelsComponent implements OnInit, OnDestroy {
  @Input() customStyle: {};
  @Output() setPageIdForParent = new EventEmitter<number>();
  ref:ComponentRef<any>;
  maStats: Array<MovingAverage> = [];
  parsedData: Array<MovingAverage> = [];
  listToWatch: Array<string> = [];
  isWatchlistActive = false;
  maType = 'h4';
  orderByField = 'pm';
  reverseSort = true;
  selectedPair = 'BTC';
  maRef: Subscription;
  isRefreshing: boolean = false;
  showWatchlistError: boolean = false;
  tdata=[
    ['d7','ema7'],
    ['t7','lt7'],
    ['d10','ma10'],
    ['t10','lt10'],
    ['d20','ma20'],
    ['t20','lt20'],
    ['d50','ma50'],
    ['t50','lt50'],
    ['d100','ma100'],
    ['t100','lt100'],
    ['d200','ma200'],
    ['t200','lt200'],
  ];
  constructor(private apiService: ApiService, private router: Router, public commonservice: CommonService,
    public globals: Globals, private userService: UserService, private auth: AuthService, private modalService: NgbModal) {
    this.commonservice.getCurrentUrl = this.router.url;
    if (this.commonservice.isMobile) {
      this.commonservice.navPopUp = true;
    }
  }
  public ngOnInit() {
    this.commonservice.footerShow = false;
    $(document).ready(() => {
      $(document).on("click", ".clone .fixstarmlvl,.clone .fixstarmlvlr", (e) => {
        let $id = e.target.id;
        this.addToWatchlistmlvl($id);
      })
    })
    const this$ = this;
    // this.maRef = this.auth.rsiScannerSettings$.subscribe({
    //   next(settings) {
    //     if (settings) {
    //       this$.orderByField = settings.sortBy;
    //       this$.reverseSort = settings.reverseSort;
    //       this$.rsiType = settings.type;
    //     }
    //   }
    // });

    this.auth.accountSettings$.subscribe({
      next(settings) {
        if (settings) {
          this$.selectedPair = settings.pairs;

          if (this$.isWatchlistActive !== settings.isWatchlistActive) {
            if (!this$.isWatchlistActive) {
              this$.maStats = this$.parsedData.filter(element => this$.listToWatch.includes(element.n));
            } else {
              this$.maStats = this$.parsedData;
            }
          }

          this$.isWatchlistActive = settings.isWatchlistActive;

          if (settings.watchlist) {
            this$.listToWatch = settings.watchlist;
            if (this$.isWatchlistActive) {
              this$.maStats = this$.parsedData.filter(element => this$.listToWatch.includes(element.n));
            }
          }
        }
      }
    });
    setTimeout(() => {
      this.getMovingAverages();
    }, 2000)
  }

  ngOnDestroy(): void {
    if (this.maRef) {
      this.maRef.unsubscribe();
    }
  }
  refreshFilters() {
    this.isRefreshing = true;
    this.isWatchlistActive = false;
    this.watchlistPressedAll();
    this.maType = 'h4';
    this.orderByField = 'pm';
    this.reverseSort = true;
    this.selectedPair = 'BTC';
    setTimeout(()=>{this.isRefreshing=false},300)
    setTimeout(()=>{
      $(".clone").remove();
      $(".main-table").clone(true, true).appendTo('#table-scroll').addClass('clone');  
    },50)
  }
  public getMovingAverages() { 
    this.apiService.getMovingAverageLevels().subscribe((data: string) => {
      const atobString = atob(data);
      const decodedString = this.globals.encryptXor(atobString);
      this.parsedData = JSON.parse(decodedString);
      if (this.isWatchlistActive) {
        this.maStats = this.parsedData.filter(element => this.listToWatch.includes(element.n));
      } else {
        this.maStats = this.parsedData;
      }
      if(this.maStats.length == 0){
        this.showWatchlistError = true;
      }
      $(document).ready(function() {
        $(".clone").remove();
        $(".main-table").clone(true, true).appendTo('#table-scroll').addClass('clone');   
     });
    });
  }

  public isWatched = (coin) => {
    return this.listToWatch.includes(coin);
  }

  addToWatchlistmlvl(coin) {
    if (this.isWatched(coin)) {
      this.listToWatch = this.listToWatch.filter((ele) => {
        return ele !== coin;
      });

      if (this.isWatchlistActive) {
        this.maStats = this.parsedData.filter(element => this.listToWatch.includes(element.n));
      }
    } else {
      this.listToWatch.push(coin);
    }

    this.userService.addToWatchlist(this.listToWatch);
    if (this.isWatchlistActive) {
      this.maStats = this.parsedData.filter(element => this.listToWatch.includes(element.n));
    }
    setTimeout(()=>{
      $(".clone").remove();
      $(".main-table").clone(true, true).appendTo('#table-scroll').addClass('clone');  
    },10)
  }

  watchlistPressedAll() {
      this.maStats = this.parsedData;
      this.isWatchlistActive = false;
      this.userService.updateAccoutSettingsField(this.isWatchlistActive, 'isWatchlistActive');
      this.showWatchlistError = false;
      setTimeout(()=>{
        $(".clone").remove();
        $(".main-table").clone(true, true).appendTo('#table-scroll').addClass('clone');  
      },10)
  }
 
  watchlistPressedStar() {
      this.maStats = this.parsedData.filter(element => this.listToWatch.includes(element.n));
      this.isWatchlistActive = true;
      this.userService.updateAccoutSettingsField(this.isWatchlistActive, 'isWatchlistActive');
      if(this.maStats.length == 0){
        this.showWatchlistError = true;
      }else{
        this.showWatchlistError = false;
      }
      setTimeout(()=>{
        $(".clone").remove();
        $(".main-table").clone(true, true).appendTo('#table-scroll').addClass('clone');  
      },10)
  }
  watchlistPressed() {
    if (!this.isWatchlistActive) {
      this.maStats = this.parsedData.filter(element => this.listToWatch.includes(element.n));
      this.isWatchlistActive = true;
    } else {
      this.maStats = this.parsedData;
      this.isWatchlistActive = false;
    }

    this.userService.updateAccoutSettingsField(this.isWatchlistActive, 'isWatchlistActive');
    setTimeout(()=>{
      $(".clone").remove();
      $(".main-table").clone(true, true).appendTo('#table-scroll').addClass('clone');  
    },10)
  }
  mobileClick(item) {
    item.isExpanded = !item.isExpanded;
  }

  public maClass = value => {
    if (value >= 0) {
      return 'txt_green';
    } else {
      return 'txt_pink';
    }
  }

  public peakMoveClass = value => {
    if (value > 0) {
      return 'greenpm';
    } else if (value < 0) {
      return 'redpm';
    }
  }
  public nameClass = (value) => {
    if (value == 2) {
      return 'triangle-copy-4.svg';
    } else if (value == 3) {
      return 'triangle-copy-3.svg';
    } else if (value == -2) {
      return 'triangle-copy-5.svg';
    } else if (value == -3) {
      return 'triangle-copy-2.svg';
    } else {
      return 'oval.svg';
    }
  }
  // public nameClass = (value) => {
  //   if (value === 2) {
  //     return 'green';
  //   } else if (value === 3) {
  //     return 'dgreen';
  //   } else if (value === -2) {
  //     return 'pink';
  //   } else if (value === -3) {
  //     return 'red';
  //   } else {
  //     return 'gray';
  //   }
  // }

  openPopUp($event, NgbdModalContent) {
    if ($event.target.tagName === 'IMG' || $event.target.tagName === 'img') {
        this.modalService.open(NgbdModalContent, { windowClass: 'showpopup' });
    }
}
  sortByField(field) {
    this.orderByField = field;
    this.reverseSort = !this.reverseSort;

    this.userService.updateField(this.orderByField, 'sortBy', UserPreferenceType.MaScanner);
    this.userService.updateField(this.reverseSort, 'reverseSort', UserPreferenceType.MaScanner);
    setTimeout(()=>{
      $(".clone").remove();
      $(".main-table").clone(true, true).appendTo('#table-scroll').addClass('clone');  
    },10)
  }

  secondLevelSort(column) {
    this.orderByField = this.maType + '.' + column;
    this.reverseSort = !this.reverseSort;

    this.userService.updateField(this.orderByField, 'sortBy', UserPreferenceType.MaScanner);
    this.userService.updateField(this.reverseSort, 'reverseSort', UserPreferenceType.MaScanner);
    setTimeout(()=>{
      $(".clone").remove();
      $(".main-table").clone(true, true).appendTo('#table-scroll').addClass('clone');  
    },10)
  }

  changePair(value) {
    this.selectedPair = value;
    this.userService.updateAccoutSettingsField(value, 'pairs');
    // $(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
    setTimeout(()=>{
      $(".clone").remove();
      $(".main-table").clone(true, true).appendTo('#table-scroll').addClass('clone');  
    },10)
  }
  changeType(value) {
    this.maType = value;
    this.userService.updateField(value, 'type', UserPreferenceType.MaScanner);
    setTimeout(()=>{
      $(".clone").remove();
      $(".main-table").clone(true, true).appendTo('#table-scroll').addClass('clone');  
    },10)
  }
  changeComponent(value){
    this.setPageIdForParent.emit(value)
  }
  checkClass(index){
    return index==0?"longLengthLoader":index==1||index==13||index==14||index==15?"MediumLengthLoader":"smallLengthLoader";
  }
}
