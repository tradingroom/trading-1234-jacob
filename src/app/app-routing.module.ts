import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/*========= All Components ===========*/
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ChatComponent } from './chat/chat.component';
import { PriceactionComponent } from './priceaction/priceaction.component';
import { RsiscannerComponent } from './rsiscanner/rsiscanner.component';
import { VolumescannerComponent } from './volumescanner/volumescanner.component';
import { CanReadGuard } from './guards/can-read.guard';
import { CanReadPaidContentGuard } from './guards/can-read-paid-content.guard';
import { LoginGuard } from './guards/login.guard';
import { LanguageComponent } from './language/language.component';
import { FibolevelsComponent } from './fibolevels/fibolevels.component';
import { MalevelsComponent } from './malevels/malevels.component';
import { BlogComponent } from './blog/blog.component';
import { MacrossoverComponent } from './macrossover/macrossover.component';
import { PaymentHistoryComponent } from './admin/payment-history/payment-history.component';
// Jamal comment start
// import { SettingsComponent } from './settings/settings.component';
// import { ExchangesComponent } from './exchanges/exchanges.component';
// Jamal comment End
import { ScannersComponent } from './marketscanners/scanners.component';
// Jamal comment start
// import { BlogDetailComponent } from './blog/blog-detail/blog-detail.component';
// import { BlogEditComponent } from './blog/blog-edit/blog-edit.component';
// Jamal comment End

const routes: Routes = [
  { path: '', component: HomeComponent , pathMatch: 'full', canActivate: [LoginGuard] },
  { path: 'login', component: LoginComponent  }, //, canActivate: [LoginGuard] },
  { path: 'home', component: HomeComponent , canActivate: [LoginGuard] },
  { path: 'pricing', component: HomeComponent , canActivate: [LoginGuard] },

  { path: 'language', component: LanguageComponent },
  { path: 'register', component: RegisterComponent   }, //, canActivate: [LoginGuard] },
  { path: 'forgotpassword', component: ForgotpasswordComponent },
  { path: 'chat', component: ChatComponent, canActivate: [CanReadPaidContentGuard] },
  { path: 'priceaction', component: PriceactionComponent, canActivate: [CanReadPaidContentGuard] },
  { path: 'screenscanners', component: ScannersComponent, canActivate: [CanReadPaidContentGuard] },
  { path: 'rsiscanner', component: RsiscannerComponent, canActivate: [CanReadPaidContentGuard] },
  { path: 'fiboscanner', component: FibolevelsComponent, canActivate: [CanReadPaidContentGuard] },
  { path: 'mascanner', component: MalevelsComponent, canActivate: [CanReadPaidContentGuard] },
  { path: 'mascannercross', component: MacrossoverComponent, canActivate: [CanReadPaidContentGuard] },
  { path: 'volumescanner', component: VolumescannerComponent, canActivate: [CanReadPaidContentGuard] },
  { path: 'subscription', component: ChatComponent, canActivate: [CanReadPaidContentGuard] },

  { path: 'blog', component: BlogComponent, canActivate: [CanReadPaidContentGuard] },
  // { path: 'blog-list', component: BlogListingComponent, canActivate: [CanReadGuard] },
  // Jamal comment start
  // { path: 'blog-detail', component: BlogDetailComponent, canActivate: [CanReadGuard] },
  // { path: 'blog-edit', component: BlogEditComponent, canActivate: [CanReadPaidContentGuard] },
  // Jamal comment End

  // { path: 'blog-page/:slug', component: BlogpageComponent, canActivate: [CanReadPaidContentGuard] },

  // Jamal comment start
  // { path: 'referral', component: SettingsComponent, canActivate: [CanReadGuard] },
  // { path: 'settings', component: SettingsComponent, canActivate: [CanReadGuard] },
  // { path: 'exchange', component: ExchangesComponent, canActivate: [CanReadGuard] },
  // Jamal comment End

  { path: 'package-manager', component: ChatComponent, canActivate: [CanReadPaidContentGuard] },
  
  { path: 'payment-history', component: PaymentHistoryComponent, canActivate: [CanReadPaidContentGuard] },
  { path: 'invite/:id', redirectTo: '/register', pathMatch: 'prefix' },
  { path: 'wallet', component: ChatComponent, canActivate: [CanReadPaidContentGuard] },

  { path: '**', redirectTo: '/home' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
