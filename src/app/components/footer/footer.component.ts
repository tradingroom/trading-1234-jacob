import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../core/common.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  today = new Date();
  constructor( public commanService: CommonService ) { }

  ngOnInit() {
  }

}
