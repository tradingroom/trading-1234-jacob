export class FiboLevels {
    n: string;
    cp: number;
    f0: number;
    f2: number;
    f3: number;
    f5: number;
    f6: number;
    f7: number;
    f1: number;
    f16: number;
    ma12: number;
    td: number;
    isExpanded = false;
}
