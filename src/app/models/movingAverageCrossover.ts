export class MovingAverageCrossover {
    n: string;
    i: string;
    cp: number;
    mk: number;
    m5: MaCrossoverRaw;
    m15: MaCrossoverRaw;
    m30: MaCrossoverRaw;
    h1: MaCrossoverRaw;
    h4: MaCrossoverRaw;
    h6: MaCrossoverRaw;
    h12: MaCrossoverRaw;
    d1: MaCrossoverRaw;
    isExpanded = false;
}

export class MaCrossoverRaw {
    // tslint:disable-next-line:variable-name
    t7_26: number;
    // tslint:disable-next-line:variable-name
    d7_26: number;
    // tslint:disable-next-line:variable-name
    t7_100: number;
    // tslint:disable-next-line:variable-name
    d7_100: number;
    // tslint:disable-next-line:variable-name
    t20_50: number;
    // tslint:disable-next-line:variable-name
    d20_50: number;
    // tslint:disable-next-line:variable-name
    t20_100: number;
    // tslint:disable-next-line:variable-name
    d20_100: number;
    // tslint:disable-next-line:variable-name
    t7_200: number;
    // tslint:disable-next-line:variable-name
    d7_200: number;
    // tslint:disable-next-line:variable-name
    t20_200: number;
    // tslint:disable-next-line:variable-name
    d20_200: number;
}
