export interface TransactionHistory {
  totalPages: number;
  totalRecords: number;
  recordsPerPage: number;
  withdrawals: Array<string>;
  status: boolean;
}
