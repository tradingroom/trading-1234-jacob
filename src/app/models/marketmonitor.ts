export class MarketMonitor {
  id: number;
  n: string;
  i: string;
  d: number;
  e: number;
  cp: number;
  v: number;
  dvc: number;
  lf: number;
  mc: number;
  pm: number;
  dt: number;
}
