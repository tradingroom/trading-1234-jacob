export class MovingAverage {
    n: string;
    i: string;
    cp: number;
    pc: number;
    pm: number;
    fm: number;
    v: number;
    m5: MaRaw;
    m15: MaRaw;
    m30: MaRaw;
    h1: MaRaw;
    h4: MaRaw;
    h6: MaRaw;
    h12: MaRaw;
    d1: MaRaw;
    isExpanded = false;
}

export class MaRaw {
    d7: number;
    d10: number;
    d20: number;
    d50: number;
    d100: number;
    d200: number;
}
