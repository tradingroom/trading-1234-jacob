export class Rsi {
  n: string;
  i: string;
  cp: number;
  pc: number;
  pm: number;
  fm: number;
  v: number;
  rsi7: RsiRaw;
  rsi14: RsiRaw;
  isExpanded = false;
}

export class RsiRaw {
  rsi5m: number;
  rsi15m: number;
  rsi30m: number;
  rsi1h: number;
  rsi4h: number;
  rsi6h: number;
  rsi12h: number;
  rsi1d: number;
}
