export class User {
  uid: string;
  email: string;
  displayName?: string;
  initial?: string;
  isNightMode: boolean;
  userName: string;
  color?: string;
  roles: Roles;
  expiration: number;
  packageId: string;
}

export interface Roles {
  free: boolean;
  subscriber: boolean;
  editor: boolean;
  admin: boolean;
}
