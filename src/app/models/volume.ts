export class Volume {
  n: string;
  i: string;
  cp: number;
  mc: number;
  v: number;
  d1: number;
  d3: number;
  d7: number;
  mv: number;
  pm: number;
  fm: number;
  isExpanded = false;
}
