export class PriceActionConfig {
    type: string;
    fibo: string;
    slider: string;
    orderByField: string;
    reverseSort: boolean;
}

export class RsiConfig {
    type: string;
    orderByField: string;
    reverseSort: boolean;
}

export class VolumeConfig {
    slider: string;
    orderByField: string;
    reverseSort: boolean;
}

export class TradingRoomConfig {
    pair: string;
    coinPair: string;
    backgroundMode: string;
    listToWatch: Array<string>;
    isWatchlistActive: boolean;
}
