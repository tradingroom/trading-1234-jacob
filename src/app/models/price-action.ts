export class PriceAction {
  n: string;
  i: string;
  cp: number;
  mc: number;
  sm5: number;
  sm15: number;
  sm30: number;
  sh1: number;
  sh4: number;
  sh6: number;
  sh12: number;
  sd1: number;
  v: number;
  peakStats: PeakStats;
  isExpanded = false;
}

export class PeakStats {
  h6: PeakValue;
  h12: PeakValue;
  d1: PeakValue;
  d2: PeakValue;
  d3: PeakValue;
  d7: PeakValue;
  d15: PeakValue;
  d30: PeakValue;
  d100: PeakValue;
  d200: PeakValue;
  d500: PeakValue;
}

export class PeakValue {
  pg: number;
  pl: number;
  llcmp: number;
  hhcmp: number;
  v: number;
}

