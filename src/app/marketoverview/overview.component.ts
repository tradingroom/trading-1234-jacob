import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from '../core/api.service';
import { CommonService } from '../core/common.service';
import { Globals } from '../core/globals';
import { Router } from '@angular/router';
import { UserService, UserPreferenceType } from '../core/user.service';
import { AuthService } from '../core/auth.service';
import { MovingAverage } from '../models/movingAverage';
import { ActivatedRoute } from "@angular/router";
@Component({
    selector: 'app-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
    //@Input() getPageIdFromChilds: number;
    activeBtn = 1;
    headerLabel = "generic.rsi_scanner";
    style1 = {
        //paddingTop: '20px',
        //backgroundColor: "green",
      }
      myBoundProperty: number;
      coins1 = [
          ['ONE',0.00000381,"+ 61.12","peak_gains"],['SNM',0.00000348,"- 12.62","peak_drop"],['DENT',"0.00000023","+ 2.88","peak_gains"],['GXS',0.00018690,"- 7.28","peak_drop"],['CELR',0.00000253,"- 1.32","peak_drop"]
      ]
      coins2 = [
          ['MATIC',0.00000313,24.12,61.8],['FET',0.00002696,12.62,38.2],['ENG',0.00006738,19.38,8.2],['TFUEL',"0.00000186",15.28,15.2],['NPXS',"0.00000012","10.00",45.2]
      ]
      coins3 = [
          ['FUEL',"2 057 BTC",801.38],['ARK',"158 BTC",303.36],['ENG',"179 BTC",240.88],['FET',"4 437 BTC",143.28],['NCASH',"39 BTC",125.93]
      ]
    constructor(private apiService: ApiService, private route: ActivatedRoute, private router: Router, public commonservice: CommonService,
        public globals: Globals, private userService: UserService, private auth: AuthService) { }

    ngOnInit() { 
        // console.log(this.route.snapshot.params.pageID,"mkbmc")
    }
    public checkOval = value => {
        if (value >= 23.6 && value <= 50) {
          return 'gradTealOval';
        } else if (value > 50 && value <= 78.6) {
          return 'gradPinkOval';
        } else {
          return 'gradnullOval';
        }
      }
    changeComponent(activeButton, headerLabel){
        this.activeBtn = activeButton;
        this.headerLabel = headerLabel;
    }
    updateValue(event){
        this.activeBtn = event;
    }
}