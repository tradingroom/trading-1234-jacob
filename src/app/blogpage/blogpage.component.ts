import { Component, OnInit } from '@angular/core';
import { CommonService } from '../core/common.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-blogpage',
  templateUrl: './blogpage.component.html',
  styleUrls: ['./blogpage.component.scss']
})
export class BlogpageComponent implements OnInit {

  public urlSlug: string;
  public API = 'https://www.admin.tradingabcd.com/';
  public blogDetaisl;
  public relatedBlogs;
  public BlogImageUrl = 'https://www.admin.tradingabcd.com/assets/uploads/blog/';

  constructor(private route: ActivatedRoute, public router: Router, public commonservice: CommonService, private http: HttpClient) {
    // console.log('blog page constructor');
    this.commonservice.getCurrentUrl = this.router.url;
    if (this.commonservice.isMobile) {
      this.commonservice.navPopUp = true;
    }
    this.route.params.forEach((urlParameters) => {
      this.urlSlug = urlParameters['slug'];
      const headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Headers', 'Content-Type');
      headers.append('Access-Control-Allow-Methods', 'GET');
      headers.append('Access-Control-Allow-Origin', '*');
      this.http.get(this.API + 'blog_detail/getBlogData?slug=' + this.urlSlug, { headers }).subscribe(res => {
        this.blogDetaisl = res[0];
        this.getRelatedBlogs(this.blogDetaisl);
      }, (err) => {

      });
    });

  }

  ngOnInit() {
    // console.log('blog page oninit');
  }

  getRelatedBlogs(blogDetaisl) {
    const headers = new HttpHeaders();
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'GET');
    headers.append('Access-Control-Allow-Origin', '*');
    this.http.get(this.API + 'blog_detail/getrelatedblog?blog_id=' + blogDetaisl.id, { headers }).subscribe(res => {
      // console.log(res);
      this.relatedBlogs = res;
    }, (err) => {
      // console.log(err);
    });
  }

  goDetaiil(blog) {
    this.router.navigate(['/blog-page', blog.slug]);
  }
}
