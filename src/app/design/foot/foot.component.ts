import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../core/common.service";

@Component({
  selector: 'app-foot',
  templateUrl: './foot.component.html',
  styleUrls: ['./foot.component.scss']
})
export class FootComponent implements OnInit {

  constructor(public commanService:CommonService ) { }

  ngOnInit() {
  }

}
