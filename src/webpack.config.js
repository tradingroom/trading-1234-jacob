var webpack = require('webpack');
var JavaScriptObfuscator = require('webpack-obfuscator');

module.exports = {
    plugins: [
        // minify output
        new webpack.optimize.UglifyJsPlugin(),
        new JavaScriptObfuscator({
            rotateUnicodeArray: true
        })
    ]
};