export const environment = {
  production: true,
  apiURL: 'https://tradingsignal.org/api',
  refLink: 'https://tradingsignal.org/invite/',
  firebase: {
    apiKey: "AIzaSyBu4yYEHUrC1PUakC7PF81pi80j3vdhv6w",
    authDomain: "tradingabcd.firebaseapp.com",
    databaseURL: "https://tradingabcd.firebaseio.com",
    projectId: "tradingabcd",
    storageBucket: "tradingabcd.appspot.com",
    messagingSenderId: "135783673622",
    appId: "1:135783673622:web:6becf4427c7ac8ea206a37"
  }
};
