// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiURL: 'https://www.tradingabcd.com/api',
  refLink: 'localhost:4200/invite/',
  firebase: {
    apiKey: "AIzaSyBu4yYEHUrC1PUakC7PF81pi80j3vdhv6w",
    authDomain: "tradingabcd.firebaseapp.com",
    databaseURL: "https://tradingabcd.firebaseio.com",
    projectId: "tradingabcd",
    storageBucket: "tradingabcd.appspot.com",
    messagingSenderId: "135783673622",
    appId: "1:135783673622:web:6becf4427c7ac8ea206a37"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
