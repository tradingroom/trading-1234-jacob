import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public firedataUsername;
  public firedataUserData;
  public colorlist = ["#13A288", "#C9424C", "#E00572", "#01BF86", "#24ABB0", "#2076AE", "#D09657", "#C75161", "#7592AE", "#9B9B9B", "#119B4F", "#F5A623", "#F18939", "#4A90E2", "#9A6CFD", "#15A2FF", "#1E87F0", "#33BBFF", "#F0506E", "#F18939"];

  constructor(public afAuth: AngularFireAuth, public fdb: AngularFireDatabase) {
    this.firedataUsername = this.fdb.database.ref('/Usernames');
    this.firedataUserData = this.fdb.database.ref('/Allusers');
  }

  doFacebookLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.FacebookAuthProvider();
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  doTwitterLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.TwitterAuthProvider();
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  doGoogleLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  doRegister(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
      .then(res => {
        this.setDisplaynameProfilePic(value.fullname);
        let uid = res.user.uid;
        let email = res.user.email;
        let username = value.username;
        let displayName = value.fullname;
        let randColorcode = this.colorlist[Math.floor(Math.random() * this.colorlist.length)];
        this.firedataUsername.child(username).set(uid);
        this.firedataUserData.child(uid).set({
            uid: uid,
            displayName: displayName,
            userName: username,
            email: email,
            createdAt: this.timestamp,
            initial: this.getInitials(displayName),
            color: randColorcode,
            // ------- In Next Step ----------
            //photoURL:'',
            //gender: '',
            //investSize: '',
            //country: 224,
            //step2: false,
            isNightMode: true,
            //about: ''
        }).then(() => {
          
        }).catch((err) => {
          
        })

        resolve(res);
      }, err => reject(err));
    });
  }

  getInitials(name) {
    var parts = name.split(' ')
    var initials = ''
    for (var i = 0; i < parts.length; i++) {
      if (parts[i].length > 0 && parts[i] !== '') {
        initials += parts[i][0]
      }
    }
    return initials
  }

  // Send email verfificaiton when new user sign up
  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
    .then(() => {
    })
  }

  setDisplaynameProfilePic(displayName){
    return this.afAuth.auth.currentUser.updateProfile({
      displayName: displayName,
      photoURL:'http://sommmerce.com/assets/images/defaultuser.png'
    }).then(() => {
      this.SendVerificationMail(); // Sending email verification notification, when new user registers
    })
  }

  get timestamp() {
    return firebase.database.ServerValue.TIMESTAMP;
  }

  checkUsernameExist(userNameId){
    var username;
    return new Promise((resolve,reject)=>{
      this.firedataUsername.child(userNameId).once('value',(snapshot)=>{
        username = snapshot.val();
      }).then(()=>{
        resolve({uname:username});
      }).catch((err)=>{
        reject(err);
      });
    })
  }

  doLogin(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
      .then(res => {
        resolve(res);
      }, err => reject(err));
    });
  }

  doLogout() {
    return new Promise((resolve, reject) => {
      if (firebase.auth().currentUser) {
        this.afAuth.auth.signOut();
        resolve();
      } else {
        reject();
      }
    });
  }

  doForgot(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().sendPasswordResetEmail(value.email)
      .then(res => {
        resolve(res);
      }, err => reject(err));
    });
    
  }
}
